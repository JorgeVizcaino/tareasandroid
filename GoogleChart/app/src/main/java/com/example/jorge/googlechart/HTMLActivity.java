package com.example.jorge.googlechart;

    import android.support.v7.app.AppCompatActivity;
    import android.os.Bundle;
    import android.webkit.WebChromeClient;
    import android.webkit.WebSettings;
    import android.webkit.WebView;
    import android.webkit.WebViewClient;


    public class HTMLActivity extends AppCompatActivity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {

            createHTML();
        }


        public void createHTML(){

            // Obtain reference to the WebView holder
            WebView webview = (WebView) this.findViewById(R.id.webview);

            // Get the settings
            WebSettings webSettings = webview.getSettings();

            // Enable Javascript for user interaction clicks
            webSettings.setJavaScriptEnabled(true);

            // Display Zoom Controles
            webSettings.setBuiltInZoomControls(true);
            webview.requestFocusFromTouch();

            // Set the client
            webview.setWebViewClient(new WebViewClient());
            webview.setWebChromeClient(new WebChromeClient());

            // Load the URL
            webview.loadUrl("file:///android_assets/GoogleChartHt.html");
        }
    }

