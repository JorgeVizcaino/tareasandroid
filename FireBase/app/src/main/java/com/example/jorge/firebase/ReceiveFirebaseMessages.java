package com.example.jorge.firebase;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class ReceiveFirebaseMessages extends FirebaseMessagingService {
    public ReceiveFirebaseMessages() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Return the communication channel to the service.
        Toast.makeText(this, "Notification Message Body: " + remoteMessage.getNotification().getBody(), Toast.LENGTH_LONG).show();

    }

}
